
var statesQueue = {
    "StateAwait":{
        'description':qsTr("На очереди"),
        'number':0
    },
    "StateExternal": {
        'description':qsTr("Композиция ВНЕ ОЧЕРЕДИ (500 руб)"),
        'number':1
    },
    "StateCurrent": {
        'description':qsTr("Сейчас поёт"),
        'color': "green",
        'number':2
    },
    "StateNextInQueue": {
        'description':qsTr("Готовиться к исполнению"),
        'number':3
    },
    "StateSplitter": {
        'description':"",
        'number':4
    }
};

function getState(index){
    var state = [
                "StateAwait",
                "StateExternal",
                "StateCurrent",
                "StateNextInQueue",
                "StateSplitter"];
    return state[index];
}
function getIndex(stateName){
    return statesQueue[stateName].number;
}

function getDescription(state){
    return statesQueue[state].description;
}
function updateAllState(model){
    //    console.log("updateAllState")
    if (model.count === 0)
        return;
    var i = 0, item;
    //для первого элемента
    item = model.get(i);
    if (item.stateQueue !== 2 && item.stateQueue !==4){
        item.stateQueue = 2;
        model.set(i, item);
    }
    ++i;
    var isNext = false;
    for (; i < model.count; ++i){
        item = model.get(i)
        if (item.stateQueue === 4)
            continue;
        //проверяем не вип
        if (item.stateQueue != 1){
            //найден готовящийся
            if (!isNext){
                isNext = true
                if (item.stateQueue !== 3){
                    item.stateQueue = 3;
                    model.set(i, item);
                }
            } else if (item.stateQueue !== 0){
                item.stateQueue = 0;
                model.set(i, item);
            }
        }
    }
    //    for (i = 0; i < model.count; ++i ){
    //        item = model.get(i)
    //        console.log(i, " ", getDescription(item.stateQueue));
    //    }
}
