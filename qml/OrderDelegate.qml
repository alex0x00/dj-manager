import QtQuick 2.7
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.1
import "../order.js" as K

Image {
    id: root
    property int stateQueue
    property string numberTable
    property string musicName
    signal modify()

    source: "qrc:/background.png"
    state: K.getState(stateQueue)
    height: 150
    width: 500 // ListView.width

    Image{
        id: img1
        anchors.fill: parent
        source: "qrc:/splitter.png"
        visible: false
    }
    Rectangle {
        id: rectangleFrame
        anchors.margins: 1
        anchors.fill: parent
        border.width: 7
        color: "#00ffffff"
        MouseArea {
            anchors.fill: parent
            acceptedButtons: Qt.RightButton
            Connections {
                onClicked: root.modify()
            }
        }
    }
    Item{
        id: content
        anchors{
            margins: rectangleFrame.border.width
            top: parent.top
            left: parent.left
            bottom: parent.bottom
        }
        Text {
            id: textTable
            color: "#ffffff"
            text: qsTr("%1 стол").arg(numberTable)
            anchors{
                margins: rectangleFrame.border.width
                top: parent.top
                left: parent.left
            }
            font.family: "Tahoma"
            font.pixelSize: parent.height/2
        }
        Text {
            id: textMusicName
            color: "#ffffff"
            text: musicName
            anchors{
                margins: rectangleFrame.border.width
                rightMargin: rectangleFrame.border.width
                bottom: parent.bottom
                left: parent.left
            }
            font.pixelSize: parent.height/4
        }
    }
    Item {
        id: item1
        anchors{
            bottom: parent.bottom
            margins: rectangleFrame.border.width
            top: parent.top
            right: parent.right
        }
        width: height * 1.25
        Text {
            id: text2

            onContentHeightChanged: {
                height = Math.min(parent.height/2, contentHeight)
            }

            color: rectangleFrame.border.color
            anchors{
                top: parent.top
                right: parent.right
                left: image.left
                //margins: rectangleFrame.border.width
                margins: 0
            }
            wrapMode: Text.WordWrap
            fontSizeMode: Text.Fit
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font{
                capitalization: Font.MixedCase
                bold: true
                family: "Times New Roman"
                pointSize: 18
            }
        }
        Image {
            id: image
            width: parent.width
            mipmap: true
            smooth: true
            anchors{
                margins: 0
                right: parent.right
                bottom: parent.bottom
                bottomMargin: 2
                top: text2.bottom
            }
            antialiasing: true
            fillMode: Image.PreserveAspectFit
            source: "qrc:/Push_logo_white.png"
        }
    }
    states: [
        State {
            //разделитель
            name: "StateSplitter"
            PropertyChanges {
                target: text2
                text: K.getDescription("StateSplitter")
            }
            PropertyChanges {
                target: content
                visible: false
            }
            PropertyChanges {
                target: img1
                visible: true
            }
            PropertyChanges {
                target: image
                visible: false
            }
        },
        State {
            //след. в очереди
            name: "StateNextInQueue"
            PropertyChanges {
                target: img1
                visible: false
            }
            PropertyChanges {
                target: text2
                text: K.getDescription("StateNextInQueue")
            }
            PropertyChanges {
                target: rectangleFrame
                border.color: "#f8f847"
            }
            PropertyChanges {
                target: image
                visible: true
            }
        },
        State {
            //текущий
            name: "StateCurrent"
            PropertyChanges {
                target: img1
                visible: false
            }
            PropertyChanges {
                target: text2
                text: K.getDescription("StateCurrent")
            }
            PropertyChanges {
                target: rectangleFrame
                border.color: "#5ac943"
            }
            PropertyChanges {
                target: image
                visible: true
            }
        },
        State {
            //вне очереди, внешний
            name: "StateExternal"
            PropertyChanges {
                target: img1
                visible: false
            }
            PropertyChanges {
                target: text2
                text: K.getDescription("StateExternal")
            }
            PropertyChanges {
                target: rectangleFrame
                border.color: "#f01f14"
            }
            PropertyChanges {
                target: image
                visible: true
            }
        },
        State {
            //в ожидании
            name: "StateAwait"
            PropertyChanges {
                target: img1
                visible: false
            }
            PropertyChanges {
                target: text2
                text: K.getDescription("StateAwait")
            }
            PropertyChanges {
                target: rectangleFrame
                border.color: "#c6b9b9"
            }
            PropertyChanges {
                target: image
                visible: true
            }
        }
    ]
}
