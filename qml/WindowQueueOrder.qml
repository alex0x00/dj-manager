import QtQuick 2.4
import QtQuick.Window 2.0

/*
Window{
    id:root
    property alias model: orderForm.model
    title: qsTr("Очередь заказов")
    Item{
        anchors.fill: parent
        ViewOrder{
            id:orderForm
            clip: true
            anchors.fill: parent
            delegate: OrderDelegate{
                width: orderForm.width
            }
        }
        Keys.onEscapePressed: {root.visible = false}
    }
}
*/
Window{
    id: subWindow
    title: qsTr("Очередь заказов")
    property alias model: orderForm.model

    ViewOrder{
        id:orderForm
        clip: true
        anchors.fill: parent
        delegate: OrderDelegate{
            stateQueue: model.stateQueue
            numberTable: model.numberTable
            musicName: model.musicName
            width: orderForm.width
        }
        Keys.onEscapePressed: {subWindow.visible = false}
    }
}
