import QtQuick 2.0

OrderForm {
    onCheckBox1Checked: {
        stateQueue = checked ? "StateExternal" :"StateAwait"
    }

    onCheckBox2Checked: {
        if (checked){
            stateQueue = "StateSplitter"
            state = "StateDisable"
        } else {
            state = "StateEnable"
            stateQueue = "StateAwait"
        }
    }

    onStateQueueChanged: {
        if (stateQueue === "StateSplitter"){
            state = "StateDisable"
        } else {
            state = "StateEnable"
            if (stateQueue === "StateExternal"){
                checkBox1.checked = true;
            } else {
                checkBox1.checked = false;
            }
        }
    }

}
