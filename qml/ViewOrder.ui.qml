import QtQuick 2.6
import QtQuick.Controls 2.0

//ListView{
//    id:listView
//    signal pressAndHold(int index)
//    focus: true
//    boundsBehavior: Flickable.StopAtBounds
//    ScrollBar.vertical: ScrollBar { size: 0.3; active: true}
//    anchors.fill: parent
//}

Flickable{
    id:listView
    property alias model: repeter.model
    property alias delegate: repeter.delegate
    signal pressAndHold(int index)
    interactive: true
    contentHeight: view3.height
    clip: true
    ScrollBar.vertical: ScrollBar { size: 0.3; active: true}

    Column{
        id: view3
        anchors {
            left: parent.left
            right: parent.right
            margins: 2
        }
        spacing: 4
        focus: true
        Repeater{
            id:repeter
        }
    }
}
