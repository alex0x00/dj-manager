import QtQuick 2.7
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.1
import "../order.js" as K

Grid {
    id: grid
    property alias numberTable: textField1.text
    property alias musicName: textField2.text
    property int stateQueue
    property string placeholderText: qsTr("...")
    property alias checkBox1: checkBox1


    signal checkBox1Checked(bool checked)
    signal checkBox2Checked(bool checked)

    //rows: 5
    columns: 2

    Label {
        text: qsTr("Столик №")
        font.pointSize: 10
        Layout.alignment: Qt.AlignLeft | Qt.AlignBaseline
    }

    TextField {
        id: textField1
        focus: true
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignLeft | Qt.AlignBaseline
        placeholderText: grid.placeholderText
    }

    Label {
        text: qsTr("Композиция")
        font.pointSize: 10
        Layout.alignment: Qt.AlignLeft | Qt.AlignBaseline
    }

    TextField {
        id: textField2
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignLeft | Qt.AlignBaseline
        placeholderText: grid.placeholderText
    }
    Label {
        Layout.column: 2
        text: qsTr("Вне очереди?")
        font.pointSize: 10
    }

    CheckBox {
        id: checkBox1
        checked: false
    }

    CheckBox {
        id: checkBox2
        checked: false
        //Layout.columnSpan: 2
        //Layout.fillWidth:true
        text: qsTr("Разделитель")
    }
    states: [
        State {
            name: "StateDisable"
            PropertyChanges {
                target: textField1
                enabled: false
            }
            PropertyChanges {
                target: textField2
                enabled: false
            }
            PropertyChanges {
                target: checkBox1
                enabled: false
            }
        },
        State {
            name: "StateEnable"
            PropertyChanges {
                target: textField1
                enabled: true
            }
            PropertyChanges {
                target: textField2
                enabled: true
            }
            PropertyChanges {
                target: checkBox1
                enabled: true
            }
        }
    ]
}
