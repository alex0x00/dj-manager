import QtQuick 2.6
import "../order.js" as K;

//TODO Сделать отдельный backend, возможно на C++
ListModel{
    id: sampleModel
    property int _selected: -1

    function isSelect(){
        return _selected >= 0;
    }

    function select(index){
        _selected = index;
    }
    function item(){
        return sampleModel.get(_selected);
    }

    function add(numberTable, musicName, stateQueue){
        sampleModel.append(
                    {
                        "numberTable": numberTable,
                        "musicName": musicName,
                        "stateQueue": stateQueue
                    });
        K.updateAllState(sampleModel);
    }
    function update(numberTable, musicName, stateQueue){
        sampleModel.set(
                    _selected,
                    {
                        "numberTable": numberTable,
                        "musicName": musicName,
                        "stateQueue": stateQueue
                    });
        K.updateAllState(sampleModel);
    }
    function del(){
        sampleModel.remove(_selected)
        K.updateAllState(sampleModel)
    }

    Component.onCompleted: {
        for (var i = 0; i < 15; ++i){
            sampleModel.append({
                                   "stateQueue": 1,
                                   "numberTable": "vrtv",
                                   "musicName": "vrt vrtv"}
                               );
        }
        K.updateAllState(sampleModel)
    }
}
