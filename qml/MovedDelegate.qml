import QtQuick 2.0
import "../order.js" as K

MouseArea {
    id: dragArea
    property bool held: false
    property int indexK: index
    property Item componentItem: content

    anchors { left: parent.left; right: parent.right }
    height: content.height

    drag.target: held ? content : undefined
    drag.axis: Drag.YAxis

    onPressAndHold: {
        held = true;
        state = "drag"
    }
    onReleased: {
        held = false;
        state = "drop"
    }

    OrderDelegate{
        id: content
        stateQueue: model.stateQueue
        numberTable: model.numberTable
        musicName: model.musicName
        width: dragArea.width;

        onModify: {
            viewOrder.pressAndHold(index)
        }

        Drag.active: dragArea.held
        Drag.source: dragArea
        Drag.hotSpot{
            x: width / 2
            y: height / 2
        }
        Rectangle{
            //z:parent.z+1
            id: opacityMask
            color:"#704e9cde"
            anchors.fill: parent
            visible: false
        }
    }

    DropArea {
        anchors { fill: parent; margins: 10 }
        onEntered: {
            //console.log("Drop", drag.source.indexK, index)
            sampleModel.move(drag.source.indexK, index, 1);
            K.updateAllState(sampleModel)
            //                    visualModel.items.move(
            //                            drag.source.DelegateModel.itemsIndex,
            //                            dragArea.DelegateModel.itemsIndex)
            //                    K.updateAllState(visualModel.items)
        }
    }
    states: [
        State {
            name: "drag"
            ParentChange { target: content; parent: viewOrder }
            AnchorChanges {
                target: content
                anchors {left: undefined; right: undefined; top: undefined}
            }
            PropertyChanges { target: opacityMask; visible: true}
        },
        State {
            name: "drop"
            ParentChange { target: content; parent: dragArea }
            AnchorChanges {
                target: content
                anchors { left: dragArea.left; right: dragArea.right; top:dragArea.top;
                }
            }
            PropertyChanges { target: opacityMask; visible: false}
        }
    ]
}

