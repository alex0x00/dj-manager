import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.0
//import QtQml.Models 2.2
ApplicationWindow {
    id:app
    title: qsTr("Панель управления")
    visible: true
    minimumWidth: 640
    minimumHeight: 480

    ToolBar {
        id: toolBar
        anchors{
            right: parent.right
            rightMargin: 10
            left: parent.left
            leftMargin: 10
        }
        RowLayout{
            CheckBox{
                id:checkBoxStateVisualWindow
                text: qsTr("Отобразить")
                onCheckStateChanged: {
                    subWindow.visible =
                            (checkState === Qt.Unchecked)
                            ? false: true;
                }
            }
            ToolButton {
                id: toolButton
                text: qsTr("Развернуть")
                onClicked: subWindow.showFullScreen()
            }
            ToolButton {
                id: toolButton1
                text: qsTr("Обычный вид")
                onClicked: {
                    subWindow.showNormal()
                    subWindow.update()
                }
            }
        }
    }

    KaraokeChartModel{
        id: sampleModel
    }

    Menu {
        id: orederMenu
        x: parent.width / 2 - width / 2
        y: parent.height / 2 - height / 2
        modal: true
        Label {
            padding: 10
            font.bold: true
            width: parent.width
            horizontalAlignment: Qt.AlignHCenter
            text: qsTr("Столик №:%1").arg(
                       sampleModel.isSelect() ?
                           sampleModel.item().numberTable:"");
        }
        MenuItem {
            text: qsTr("Редактировать")
            onTriggered: orderDialog.editOrder()
        }
        MenuItem {
            text: qsTr("Удалить")
            onTriggered: {
                sampleModel.del();
            }
        }
    }

    Component{
        id:delegate
        MovedDelegate{}
    }

//    DelegateModel {
//        id: visualModel
//        model: sampleModel
//        delegate: delegate
//    }
    ViewOrder{
        id: viewOrder
        anchors{
            top: toolBar.bottom
            topMargin: 5
            right: parent.right
            bottom: parent.bottom
            left: parent.left
        }
        model: sampleModel//visualModel
        delegate: delegate
        onPressAndHold: {
            sampleModel.select(index)
            orederMenu.open()
        }
        interactive: true
    }

    RoundButton {
        text: qsTr("+")
        highlighted: true
        anchors{
            margins: 15
            right: parent.right
            bottom: parent.bottom
        }
        onClicked: {
            orderDialog.createOrder()
        }
    }
    OrderDialog {
        id: orderDialog
    }

    WindowQueueOrder{
        id:subWindow
        title: qsTr("Очередь заказов")
        width: 800
        height: 600
        visible: false
        model: sampleModel
        onVisibleChanged: {
            checkBoxStateVisualWindow.checked = visible
        }
    }
}
