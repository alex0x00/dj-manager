import QtQuick 2.7
import QtQuick.Controls 2.1
/*
stateQueue
*/
Dialog {
    id: dialog
    title: qsTr("Добавление заказа")
    focus: true
    modal: true
    standardButtons: Dialog.Ok | Dialog.Cancel

    x: parent.width / 2 - width / 2
    y: parent.height / 2 - height / 2
    property string state

    Order {id: form}

    onAccepted: {
        if (state === "add"){
            sampleModel.add(form.numberTable, form.musicName, form.stateQueue);
        } else if (state === "edit") {
            sampleModel.update(form.numberTable, form.musicName, form.stateQueue)
        }
    }
    function createOrder() {
        form.numberTable = "";
        form.musicName = "";
        form.stateQueue = 0;

        dialog.title = qsTr("Добавление");
        state = "add"
        dialog.open();
    }

    function editOrder() {
        var order = sampleModel.item();
        form.numberTable = order.numberTable;
        form.musicName = order.musicName;
        form.stateQueue = order.stateQueue;

        dialog.title = qsTr("Редактирование");
        state = "edit"
        dialog.open();
    }
}
